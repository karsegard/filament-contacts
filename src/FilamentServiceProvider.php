<?php

namespace KDA\Filament\Contact;
use Filament\PluginServiceProvider;
use KDA\Filament\Contact\Resources\ContactResource;
use Spatie\LaravelPackageTools\Package;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
        ContactResource::class
    ];

    protected array $relationManagers = [
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-contacts');
    }
}
