<?php

namespace KDA\Filament\Contact\Resources\ContactResource\RelationManagers;

use KDA\Filament\Contact\Resources\ContactResource;
use  KDA\Laravel\Contacts\Models\Contact;
use  KDA\Laravel\Contacts\Models\ContactRole;
use Closure;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Database\Eloquent\Relations\Relation;

class ContactsRelationManager extends CompaniesRelationManager
{
    protected static string $relationship = 'contacts';
    protected bool $allowsDuplicates = true;
    protected static ?string $recordTitleAttribute = 'full_name';
    public static function canViewForRecord(Model $ownerRecord): bool
    {
        return $ownerRecord->is_company == true;
    }
    protected function getTableRecordUrlUsing(): Closure
    {
        return fn (Model $record): string => ContactResource::getUrl('edit', ['record' => $record->contact_id]);
    }
}
