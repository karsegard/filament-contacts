<?php

namespace KDA\Filament\Contact\Resources\ContactResource\RelationManagers;

use KDA\Filament\Contact\Resources\ContactResource;
use  KDA\Laravel\Contacts\Models\ContactRole;
use Closure;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Database\Eloquent\Relations\Relation;

class CompaniesRelationManager extends RelationManager
{
    
    protected static string $relationship = 'companies';
    protected bool $allowsDuplicates = true;
    protected static ?string $recordTitleAttribute = 'full_name';

    public static function canViewForRecord(Model $ownerRecord): bool
    {
        return $ownerRecord->is_company == false;
    }

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('phone'),
                Forms\Components\TextInput::make('email'),
                static::getSelectRoleField(),

                /* Select::make('company_id')
                ->options(fn ($livewire) => Contact::where('id','!=',$livewire->ownerRecord->id)->pluck('full_name', 'id'))
*/

            ]);
    }

    /* protected function getTableQuery(): Builder | Relation
    {
        $query = parent::getTableQuery();

     //   return $query->with('contacts.role');
        return $query;
    }*/

    protected function getTableQuery(): Builder | Relation
    {
        return parent::getTableQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }

    public static function getSelectRoleField(): Select
    {
        return Select::make('role_id')
            ->options(fn () => ContactRole::all()->pluck('name', 'id'))
            ->createOptionForm(
                [
                    Forms\Components\TextInput::make('name')->label('Role')
                        ->required(),
                ]
            )
            ->createOptionUsing(function (array $data, $livewire) {
                return ContactRole::create([...$data])->getKey();
            });
    }

    public static function table(Table $table): Table
    {
        //dump(Contact::with('companies')->get());
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('full_name'),
                TextColumn::make('role')->getStateUsing(function ($record) {
                    return ContactRole::find($record->role_id)?->name;
                }),
                TextColumn::make('phone'),
                TextColumn::make('email'),

            ])
            ->filters([
                //
                Tables\Filters\TrashedFilter::make(),
            ])
            ->headerActions([
                
                Tables\Actions\CreateAction::make()
                    ->form([
                        Toggle::make('is_company')->columnSpan('full')->reactive(),
                        Fieldset::make('Contact')->schema([
                            TextInput::make('title'),
                            TextInput::make('firstname'),
                            TextInput::make('lastname'),
                        ])->columnSpan(1)->when(fn($get)=>$get('is_company') !=true),
                        Fieldset::make('Contact')->schema([
                            TextInput::make('firstname')->label('name')->columnSpan('full'),
                        ])->columnSpan(1)->when(fn($get)=>$get('is_company') ==true),
                        Fieldset::make('Relation')->schema([
                            TextInput::make('phone'),
                            TextInput::make('email'),
                            static::getSelectRoleField()
                        ])
                        
                    ]),
                Tables\Actions\AttachAction::make()
                    ->preloadRecordSelect()
                    ->form(fn (Tables\Actions\AttachAction $action): array => [
                        $action->getRecordSelect()->label('Company')->required(),
                        TextInput::make('phone'),
                        TextInput::make('email'),
                        static::getSelectRoleField()
                    ]),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\DetachAction::make(),
                Tables\Actions\DeleteAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DetachBulkAction::make(),
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    protected function getTableRecordUrlUsing(): Closure
    {
        return fn (Model $record): string => ContactResource::getUrl('edit', ['record' => $record->company_id]);
    }
}
