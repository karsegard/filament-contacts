<?php

namespace KDA\Filament\Contact\Resources\ContactResource\Pages;

use KDA\Filament\Contact\Resources\ContactResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateContact extends CreateRecord
{
    protected static string $resource = ContactResource::class;
}
