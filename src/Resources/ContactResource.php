<?php

namespace KDA\Filament\Contact\Resources;

use KDA\Filament\Contact\Resources\ContactResource\Pages;
use KDA\Filament\Contact\Resources\ContactResource\RelationManagers;
use KDA\Filament\Contact\Resources\ContactResource\RelationManagers\CompaniesRelationManager;
use KDA\Filament\Contact\Resources\ContactResource\RelationManagers\ContactsRelationManager;
use Filament\Forms;
use Filament\Forms\Components\Fieldset;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use KDA\Laravel\Contacts\Models\Contact;

class ContactResource extends Resource
{
    protected static ?string $model = Contact::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                Toggle::make('is_company')->columnSpan('full')->reactive(),
                Fieldset::make('Contact')->schema([
                    TextInput::make('title'),
                    TextInput::make('firstname'),
                    TextInput::make('lastname'),
                ])->columnSpan(1)->when(fn($get)=>$get('is_company') !=true),
                Fieldset::make('Contact')->schema([
                    TextInput::make('firstname')->label('name')->columnSpan('full'),
                ])->columnSpan(1)->when(fn($get)=>$get('is_company') ==true),

                Fieldset::make('Adresse')->schema([
                    TextInput::make('address'),
                    TextInput::make('zip'),
                    TextInput::make('city'),
                    
                ])->columnSpan(1),

                Fieldset::make('Contacts')->schema([
                    TextInput::make('general_phone'),
                    TextInput::make('general_email')
                ])->columnSpan(1),
                
            ])->columns(2);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('full_name')
            ])
            ->filters([
                Tables\Filters\TrashedFilter::make(),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
                Tables\Actions\RestoreBulkAction::make(),
                Tables\Actions\ForceDeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        
        return [
            CompaniesRelationManager::class,
            ContactsRelationManager::class,
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListContacts::route('/'),
            'create' => Pages\CreateContact::route('/create'),
            'edit' => Pages\EditContact::route('/{record}/edit'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()
            ->withoutGlobalScopes([
                SoftDeletingScope::class,
            ]);
    }
}
